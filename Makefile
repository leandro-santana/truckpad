clean:
	@echo "Execute cleaning ..."
	rm -f *.pyc
	rm -f coverage.xml

pep8:
	@find . -type f -not -path "*./.venv/*" -name "*.py"|xargs flake8 --max-line-length=120 --ignore=E402 --max-complexity=6


tests: clean pep8
	py.test tests


coverage:clean pep8
	py.test --cov=truckpad --cov-report=xml:coverage.xml tests

coverage_html:clean pep8
	py.test --cov=truckpad --cov-report=html:coverage tests

sonar: coverage
	sonar-scanner
