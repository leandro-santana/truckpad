SET NAMES utf8;

DROP TABLE IF EXISTS truckpad.drivers;

CREATE TABLE  IF NOT EXISTS truckpad.drivers(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `idade` int(11) DEFAULT NULL,
  `sexo` char(1) DEFAULT NULL,
  `tipo_cnh` char(2) DEFAULT NULL,
  `tipo_veiculo` int(11) DEFAULT NULL,
  `veiculo_carregado` tinyint(1) DEFAULT '0',
  `destino` json DEFAULT NULL,
  `origem` json DEFAULT NULL,
  `data_atualizacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `data_criacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `possui_veiculo` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `drivers_trucks_type_id_fk` (`tipo_veiculo`),
  CONSTRAINT `drivers_trucks_type_id_fk` FOREIGN KEY (`tipo_veiculo`) REFERENCES `trucks_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1