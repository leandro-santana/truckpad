SET NAMES utf8;

CREATE DATABASE IF NOT EXISTS truckpad;

USE truckpad;

CREATE TABLE IF NOT EXISTS trucks_type
(
	id int auto_increment,
	tipo varchar(100) null,
	constraint trucks_type_pk
		primary key (id)
)
comment 'table to type trucks';

INSERT INTO `truckpad`.`trucks_type` (`tipo`)
VALUES
('Caminhão 3/4'),
('Caminhão Toco'),
('Caminhão Truck'),
('Carreta Simples'),
('Carreta Eixo Extendido');

CREATE TABLE `drivers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `idade` int(11) DEFAULT NULL,
  `sexo` char(1) DEFAULT NULL,
  `tipo_cnh` char(2) DEFAULT NULL,
  `possui_veiculo` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1

