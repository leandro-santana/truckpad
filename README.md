# TruckPad API

Code Challenge - TruckPad <br />

Sistema Operacional **Ubuntu 18.04**

**Docker**<br />

Como base para infraestrutura do projeto, foi utilizado o docker e docker-compose, que caso não esteja instalado seguir o procedimento abaixo:

links de referência: <br/>
[Instalação docker ce](https://docs.docker.com/install/linux/docker-ce/ubuntu/)<br/>
[Instalação docker-compose](https://docs.docker.com/compose/install/)


```
docker:
$ sudo apt-get update
$ sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -    
$ docker --version
**Docker version 18.09.3, build 774a1f4**


docker-compose:
$ sudo curl -L "https://github.com/docker/compose/releases/download/1.23.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
$ sudo chmod +x /usr/local/bin/docker-compose
$ sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
$ docker-compose --version

**docker-compose version 1.23.2, build 1110ad01**

```

Após a instalação executar o comando abaixo para levantar os ambientes.
```
$ docker-compose up
```


**SonarQube**<br />

Caso necessário a avaliação do código e sua cobertura de testes poderá ser analisado via sonarQube.

Para que que a api seja submetida ao projeto sonar seguir o processo abaixo.

```
$ cd <project dir>
$ export PATH=$(pwd)/sonar-scanner/bin:$PATH
$ docker run -d --name sonarqube -p 9000:9000 sonarqube:7.5-community
$ make sonar

```
O acompanhamento poderá ser realizado no link [http://localhost:9000](http://localhost:9000)

**TruckPad Api**<br />

Conforme perguntas abaixo seguir os endpoints e contrato de envio:

 P1: Precisamos criar uma api para para cadastrar os motoristas que chegam nesse terminal e saber mais informações sobre eles. Precisamos saber nome, idade, sexo, se possui veiculo, tipo da CNH, se está carregado, tipo do veiculo que está dirigindo.
 
 P2: Precisamos saber a origem e destino de cada caminhoneiro. Será necessário pegar a latitude e longitude de cada origem e destino.
 
 ```
 (Endpoint): http://localhost:8080/api/drivers
 (Method): POST
 (Payload para Envio):
 
 {
  "nome": "Leandro Santana",
  "idade": 35,
  "sexo": "M",
  "possui_veiculo": true,
  "tipo_cnh": "D",
  "veiculo_carregado": true,
  "tipo_veiculo": 2,
  "origem": {
    "latitude": -23.3918295,
    "longitude": -46.484414
  },
  "destino": {
    "latitude": -23.496477,
    "longitude": -46.85930727
  }
}
 ```
 
 P3: Precisamos de um método para consultar todos os motoristas que não tem carga para voltar ao seu destino de origem.
 
  ```
  Para recuperar motoristas sem carga:
  (Endpoint): http://localhost:8080/api/drivers/empty_charge
  (Method): GET
  (Resposta): 
  {
    "status": "success",
    "message": {
        "0": {
            "id": 15,
            "nome": "João Pedro",
            "idade": 35,
            "origem": {
                "latitude": -23.3918295,
                "longitude": -46.484414
            },
            "destino": {
                "latitude": -23.496477,
                "longitude": -46.85930727
            },
            "possui_veiculo": 0,
            "tipo_cnh": "D",
            "sexo": "M",
            "tipo_veiculo": 2
        }
    }
  }
  
  Para recuperar motoristas com carga:
  (Endpoint): http://localhost:8080/api/drivers/full_charge
  (Method): GET
  (Resposta): 
  {
    "status": "success",
    "message": {
        "0": {
            "id": 1,
            "nome": "João Pedro",
            "idade": 35,
            "origem": {
                "latitude": -23.3918295,
                "longitude": -46.484414
            },
            "destino": {
                "latitude": -23.496477,
                "longitude": -46.85930727
            },
            "possui_veiculo": 0,
            "tipo_cnh": "D",
            "sexo": "M",
            "tipo_veiculo": 1
        }
    }
  }    
  ```
  P4: Precisamos saber quantos caminhões passam carregados pelo terminal durante o dia, semana e mês.
     
 ```
 (Endpoint): http://localhost:8080/api/drivers/full_charge/2019/03/25/7
 (Method): GET
 
 Explicando os parâmetros:
 full_charge -> somente motoristas com carga
 2019 -> Ano do registro
 03 -> Mês do registro
 25 -> Dia do registro
 7 -> ultimos 7 dias a contas da data de origem conforme outros parametros 2019-03-25
 (Resposta):
 
 {
    "status": "success",
    "message": {
        "0": {
            "id": 1,
            "nome": "João Pedro",
            "idade": 35,
            "origem": {
                "latitude": -23.3918295,
                "longitude": -46.484414
            },
            "destino": {
                "latitude": -23.496477,
                "longitude": -46.85930727
            },
            "possui_veiculo": 0,
            "tipo_cnh": "D",
            "sexo": "M",
            "tipo_veiculo": 1
        }
    }
}    
 ```
 P5: Precisamos saber quantos caminhoneiros tem veiculo próprio.
 
```
(Endpoint): http://localhost:8080/api/drivers/own_truck
(Method): GET
(Resposta):
{
    "status": "success",
    "message": {
        "0": {
            "id": 21,
            "nome": "Leandro Santana",
            "idade": 35,
            "origem": {
                "latitude": -23.3918295,
                "longitude": -46.484414
            },
            "destino": {
                "latitude": -23.496477,
                "longitude": -46.85930727
            },
            "possui_veiculo": 1,
            "tipo_cnh": "D",
            "sexo": "M",
            "tipo_veiculo": 2
        }
    }
}        
```

P6: Mostrar uma lista de origem e destino agrupado por cada um dos tipos.
```
(Endpoint): http://localhost:8080/api/drivers/group_drivers
(Method): GET
(Retorno):
{
    "status": "success",
    "message": {
        "Caminhão 3/4": {
            "1": {
                "id": 1,
                "nome": "Leandro Santana",
                "idade": 25,
                "origem": {
                    "latitude": -23.3918295,
                    "longitude": -46.484414
                },
                "destino": {
                    "latitude": -23.496477,
                    "longitude": -46.85930727
                },
                "possui_veiculo": 0,
                "tipo_cnh": "D",
                "sexo": "M",
                "tipo_veiculo": 1,
                "tipo": "Caminhão 3/4"
            }
        }, "Caminhão Toco": {
            "1": {
                "id": 10,
                "nome": "João Pedro",
                "idade": 35,
                "origem": {
                    "latitude": -23.3918295,
                    "longitude": -46.484414
                },
                "destino": {
                    "latitude": -23.496477,
                    "longitude": -46.85930727
                },
                "possui_veiculo": 0,
                "tipo_cnh": "D",
                "sexo": "M",
                "tipo_veiculo": 2,
                "tipo": "Caminhão Toco"
            }
        }        
    }
```

P7: Será necessário atualizar os registros dos caminhoneiros.
```
(Endpoint): http://localhost:8080/api/drivers/1 <- id 1 == código do registro
(Method): PUT
(Contrato de Atualização):

{
  "nome": "Leandro Santana",
  "idade": 25,
  "sexo": "M",
  "possui_veiculo": false,
  "tipo_cnh": "D",
  "veiculo_carregado": true,
  "tipo_veiculo": 1,
  "origem": {
    "latitude": -23.3918295,
    "longitude": -46.484414
  },
  "destino": {
    "latitude": -23.496477,
    "longitude": -46.85930727
  }
}

```

P8: Criar testes unitários para validar o funcionamento dos serviços criados. Utilize o framework de testes de sua preferência.

```
Poderá ser executado com o comando:
$ cd <dir project>
$ make tests
```