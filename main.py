import multiprocessing

from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop

from truckpad.configs import config

from truckpad import create_app

if __name__ == '__main__':
    app = create_app()

    server = HTTPServer(app)
    server.bind(config.PORT)
    server.start(multiprocessing.cpu_count() if not config.APP_DEBUG else 1)
    IOLoop.instance().start()
    app.listen(config.PORT)
    IOLoop.current().start()
