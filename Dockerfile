FROM python:3.7.2-alpine

ENV PYTHONUNBUFFERED=true
ENV C_FORCE_ROOT=true

RUN apk update \
  && apk add \
    build-base \
    libpq \
    libffi-dev \
    supervisor \
    openssh-client \
    git \
    mariadb-dev \
    linux-headers \
    alpine-sdk


RUN pip install -U pip
RUN pip install pipenv

WORKDIR /opt/truckpad
ADD ./Pipfile /opt/truckpad
ADD ./Pipfile.lock /opt/truckpad

RUN pipenv install --skip-lock

EXPOSE 8080