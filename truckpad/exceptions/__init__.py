from http import HTTPStatus


class GenericError(Exception):
    status_code = 500
    message = 'Internal Server Error'

    def __init__(self, message=None):
        if message:
            self.message = message


class InvalidConnection(GenericError):
    def __init__(self, message, status_code=HTTPStatus.NOT_FOUND.value):
        super(InvalidConnection, self).__init__(message)
        self.status_code = status_code


class UnprocessableEntity(GenericError):
    def __init__(self, message, status_code=HTTPStatus.UNPROCESSABLE_ENTITY.value):
        super(UnprocessableEntity, self).__init__(message)
        self.status_code = status_code


class ROUTENOTFOUND(GenericError):
    def __init__(self, message=HTTPStatus.NOT_FOUND.name, status_code=HTTPStatus.NOT_FOUND.value):
        super(ROUTENOTFOUND, self).__init__(message)
        self.status_code = status_code


COMMON_ERRORS = (InvalidConnection, UnprocessableEntity, ROUTENOTFOUND)
