from http import HTTPStatus

from tornado.web import HTTPError

from truckpad.integrations import TruckPadApiAbstract
from truckpad.integrations.driver_registration import DriverRegistration

__all__ = ['DriverRegistration']
LIST_ABSTRACTS = [TruckPadApiAbstract]


class TruckPadApiFactory:
    @staticmethod
    def get_instance(carrier, headers: dict = {}):
        for abstracts_classes in LIST_ABSTRACTS:
            for klass in abstracts_classes.__subclasses__():
                if carrier.lower() == klass.carrier:
                    return klass(headers=headers)
        raise HTTPError(status_code=HTTPStatus.NOT_FOUND.value, reason=HTTPStatus.NOT_FOUND.name)
