from abc import ABCMeta, abstractmethod
from marshmallow import ValidationError
from truckpad.models.trucks_type import TrucksTypeModel


class TruckPadApiAbstract(metaclass=ABCMeta):
    contract = {}

    def __init__(self, headers: dict = {}):
        self.headers = headers

    @abstractmethod
    def process(self, request):
        raise NotImplementedError

    @abstractmethod
    def get_data(self, **kwargs):
        raise NotImplementedError

    @abstractmethod
    def update_data(self, **kwargs):
        raise NotImplementedError

    @staticmethod
    def validate_type_truck(value):
        result_type_truck = TrucksTypeModel().orm.db_session.query(TrucksTypeModel).get(value)
        if not result_type_truck:
            raise ValidationError("type of truck not registered for the reported code")
