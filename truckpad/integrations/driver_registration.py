from webargs import fields

from truckpad.common.utils import object_convert, del_dict_values
from truckpad.exceptions import UnprocessableEntity, InvalidConnection
from truckpad.integrations import TruckPadApiAbstract
from truckpad.models.drivers import DriverModel
from truckpad.models.trucks_type import TrucksTypeModel
from sqlalchemy.sql import func
from sqlalchemy.sql.expression import text


class DriverRegistration(TruckPadApiAbstract):
    carrier = 'drivers'

    def __init__(self, headers: dict = {}):
        super(DriverRegistration, self).__init__(headers=headers)
        self.list_options_translate = {
            'option': {
                'empty_charge': DriverModel.veiculo_carregado == 0,
                'full_charge': DriverModel.veiculo_carregado == 1,
                'own_truck': DriverModel.possui_veiculo == 1,
                'group_drivers': ''},
            'year': DriverModel.extract_date('year', DriverModel.data_criacao),
            'month': DriverModel.extract_date('month', DriverModel.data_criacao),
            'day': DriverModel.extract_date('day', DriverModel.data_criacao),
            'week': DriverModel.data_criacao
        }

        self.contract = {
            "nome": fields.Str(required=True),
            "idade": fields.Int(required=True, validate=lambda val: val > 0),
            "sexo": fields.Str(required=True, validate=lambda val: val in ['M', 'F']),
            "possui_veiculo": fields.Bool(required=True),
            "tipo_cnh": fields.Str(required=True, validate=lambda val: val in ['A', 'AB', 'C', 'D']),
            "veiculo_carregado": fields.Bool(required=True),
            "tipo_veiculo": fields.Int(required=True, validate=self.validate_type_truck),
            "origem": fields.Nested({
                "latitude": fields.Float(required=True),
                "longitude": fields.Float(required=True),
            }, required=True),
            "destino": fields.Nested({
                "latitude": fields.Float(required=True),
                "longitude": fields.Float(required=True),
            }, required=True)
        }

    def __generate_where(self, query, kwargs):
        for key, value in kwargs.items():

            if value is None:
                continue

            value = value.lstrip('/')
            this_key = self.list_options_translate.get(key, {})

            if type(this_key) is dict and this_key.get(value) is None:
                raise UnprocessableEntity(message="The '{opt}' key is not valid".format(opt=value))

            if type(this_key) is not dict:
                query = query.filter(this_key == value)
            elif this_key.get(value) != '':
                query = query.filter(this_key.get(value))
        return query

    def __group_drivers(self, result):

        group_result = {}
        for key, values in result.items():
            if not group_result.get(values['tipo']):
                group_result.update({values['tipo']: {}})
            group_result[values['tipo']].update({len(group_result[values['tipo']]) + 1: values})
        return group_result

    def __select_week(self, query, **kwargs):

        date_find = '{year}-{month}-{day}'.format(year=kwargs.get('year').lstrip('/'),
                                                  month=kwargs.get('month').lstrip('/'),
                                                  day=kwargs.get('day').lstrip('/'))
        query = query.filter(
            DriverModel.data_criacao >= func.date_sub(date_find, text(
                'interval {week} day'.format(week=kwargs.get('week').lstrip('/')))))

        query = query.filter(
            DriverModel.data_criacao < func.date_add(date_find, text(
                'interval {week} day'.format(week=1))))

        return query

    def process(self, request: dict = {}):
        ed_driver = DriverModel(
            nome=request.get('nome'),
            idade=request.get('idade'),
            sexo=request.get('sexo'),
            tipo_cnh=request.get('tipo_cnh'),
            possui_veiculo=request.get('possui_veiculo'),
            veiculo_carregado=request.get('veiculo_carregado'),
            tipo_veiculo=request.get('tipo_veiculo'),
            origem=request.get('origem'),
            destino=request.get('destino')
        )

        ed_driver.orm.object_commit(ed_driver)

    def get_data(self, **kwargs):

        drive_model = DriverModel()
        query = drive_model.orm.db_session.query(
            DriverModel.id, DriverModel.nome, DriverModel.idade, DriverModel.origem, DriverModel.destino,
            DriverModel.possui_veiculo, DriverModel.tipo_cnh, DriverModel.sexo, DriverModel.tipo_veiculo,
            TrucksTypeModel.tipo
        ).join(TrucksTypeModel)

        if kwargs.get('week'):
            query = self.__select_week(query, **kwargs)
            kwargs = del_dict_values(kwargs, ['year', 'month', 'day', 'week'])

        query = self.__generate_where(query, kwargs)

        result = query.all()
        result = object_convert(result)

        if kwargs.get('option') == '/group_drivers':
            result = self.__group_drivers(result)
        return result

    def update_data(self, request, **kwargs):
        id_driver = kwargs.get('option').lstrip('/')

        drive_model = DriverModel()
        driver = drive_model.orm.db_session.query(DriverModel).filter(DriverModel.id == id_driver).first()

        if not driver:
            raise InvalidConnection(message='driver not found')

        driver.nome = request.get('nome')
        driver.idade = request.get('idade')
        driver.sexo = request.get('sexo')
        driver.possui_veiculo = request.get('possui_veiculo')
        driver.tipo_cnh = request.get('tipo_cnh')
        driver.veiculo_carregado = request.get('veiculo_carregado')
        driver.tipo_veiculo = request.get('tipo_veiculo')
        driver.origem = request.get('origem')
        driver.destino = request.get('destino')

        drive_model.orm.object_commit(driver)

        return driver
