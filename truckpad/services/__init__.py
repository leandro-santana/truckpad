import base64


def generate_hash(value):
    value = value.encode("ascii")
    return base64.b64encode(value).decode('utf8')


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        hash_cls = generate_hash(str(args) + str(kwargs))
        if hash_cls not in cls._instances:
            cls._instances[hash_cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[hash_cls]

    @staticmethod
    def drop():
        """Drop the instance (for testing purposes)"""
        Singleton._instances = {}
