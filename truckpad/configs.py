import os
import sys


class Config(object):
    PORT = 80
    SQL_USER = os.getenv('SQL_USER')
    SQL_PASSWORD = os.getenv('SQL_PASSWORD')
    SQL_HOST = os.getenv('SQL_HOST')
    SQL_DATABASE = os.getenv('SQL_DATABASE')
    SQL_PORT = os.getenv('SQL_PORT')
    TIMEOUT_RECONNECT_MYSQL = 3000
    POLL_SIZE_MYSQL = 20
    MYSQL_CONNECT = 'mysql+pymysql://{user}:{password}@{host}/{db}'.format(user=SQL_USER, password=SQL_PASSWORD,
                                                                           host=SQL_HOST, db=SQL_DATABASE)

    APP_DEBUG = os.environ.get('APP_DEBUG', False)


class Development(Config):
    PORT = 8080
    SQL_USER = 'root'
    SQL_PASSWORD = 'admin'
    SQL_HOST = 'truckpad.mysql'
    SQL_DATABASE = 'truckpad'
    SQL_PORT = 3306
    MYSQL_CONNECT = 'mysql+pymysql://{user}:{password}@{host}/{db}'.format(user=SQL_USER, password=SQL_PASSWORD,
                                                                           host=SQL_HOST, db=SQL_DATABASE)


class Testing(Config):
    APP_DEBUG = True


def _load_config():
    _config_class = getattr(sys.modules[__name__], os.getenv('ENVIRONMENT', 'Development'))
    return _config_class()


config = _load_config()
