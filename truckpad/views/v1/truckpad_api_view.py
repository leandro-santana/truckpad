from http import HTTPStatus

from tornado.httpclient import HTTPError
from webargs.tornadoparser import parser

from truckpad.exceptions import COMMON_ERRORS
from truckpad.integrations.factory.truckpad_api_factory import TruckPadApiFactory
from truckpad.views import ApiJsonHandler


class ApiView(ApiJsonHandler):

    def post(self, **kwargs):
        try:
            carrier = kwargs.get('carrier')
            instance = TruckPadApiFactory.get_instance(carrier=carrier, headers=dict(self.request.headers))
            request = parser.parse(instance.contract, self.request, locations=('json',))
            instance.process(request)
            self.success(status_code=HTTPStatus.CREATED.value, message=HTTPStatus.CREATED.name)
        except HTTPError as error:
            self.error(message=error.message, status_code=error.code)
        except COMMON_ERRORS as error:
            self.error(message=error.message, status_code=error.status_code)

    def get(self, **kwargs):
        try:
            carrier = kwargs.get('carrier')
            del kwargs['carrier']
            instance = TruckPadApiFactory.get_instance(carrier=carrier, headers=dict(self.request.headers))
            response = instance.get_data(**kwargs)
            self.success(status_code=HTTPStatus.OK.value, message=response)
        except HTTPError as error:
            self.error(status_code=error.code, message=error.message)
        except COMMON_ERRORS as error:
            self.error(status_code=error.status_code, message=error.message)

    def put(self, **kwargs):
        try:
            carrier = kwargs.get('carrier')
            instance = TruckPadApiFactory.get_instance(carrier=carrier, headers=dict(self.request.headers))
            request = parser.parse(instance.contract, self.request, locations=('json',))
            instance.update_data(request, **kwargs)
            self.success(status_code=HTTPStatus.OK.value, message='updated record success')
        except HTTPError as error:
            self.error(status_code=error.code, message=error.message)
        except COMMON_ERRORS as error:
            self.error(status_code=error.status_code, message=error.message)
