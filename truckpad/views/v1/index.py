import tornado


class Index(tornado.web.RequestHandler):

    def get(self):
        self.render("static/index.html", title="TruckPad API", message='Hello! This is a TruckPad API Test :)')
