from tornado.httputil import HTTPServerRequest
from tornado.web import RequestHandler


class ApiJsonHandler(RequestHandler):
    def __init__(self, *args, **kwargs):
        HTTPServerRequest()
        super().__init__(*args, **kwargs)

    def __message_default(self, type_message, status_code, message):
        result = {"status": type_message, "message": message}
        self.set_status(status_code)
        self.finish(result)

    def initialize(self):
        self.set_header("Content-Type", "application/json")

    def write_error(self, status_code, **kwargs):
        exception = str(kwargs["exc_info"][1])
        self.set_status(status_code)
        self.error(status_code, exception)

    def success(self, status_code, message):
        self.__message_default("success", status_code, message)

    def error(self, status_code, message):
        self.__message_default("error", status_code, message)
