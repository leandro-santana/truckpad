from tornado.web import url

from truckpad.views.v1.index import Index
from truckpad.views.v1.truckpad_api_view import ApiView

routes = [
    url(r"/", Index),
    url(r"/api/(?P<carrier>\w+)(?P<option>/\w+)?(?P<year>/\w+)?(?P<month>/\w+)?(?P<day>/\w+)?(?P<week>/\w+)?", ApiView)
]
