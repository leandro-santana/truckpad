# coding: utf-8
from sqlalchemy import CHAR, Column, JSON, String, TIMESTAMP, text, ForeignKey
from sqlalchemy.dialects.mysql import INTEGER, TINYINT
from sqlalchemy.orm import relationship

from truckpad.models.model_base import ModelBase
from truckpad.models.trucks_type import TrucksTypeModel


class DriverModel(ModelBase):
    __tablename__ = 'drivers'

    id = Column(INTEGER(11), primary_key=True)
    nome = Column(String(255))
    idade = Column(INTEGER(11))
    sexo = Column(CHAR(1))
    tipo_cnh = Column(CHAR(2))
    possui_veiculo = Column(TINYINT(1), server_default=text("'0'"))
    veiculo_carregado = Column(TINYINT(1))
    tipo_veiculo = Column(INTEGER(11), ForeignKey(TrucksTypeModel.id))
    origem = Column(JSON)
    destino = Column(JSON)
    data_criacao = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))
    data_atualizacao = Column(TIMESTAMP, nullable=False,
                              server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))

    veiculo = relationship(TrucksTypeModel, foreign_keys=[tipo_veiculo])
