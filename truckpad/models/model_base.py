from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import extract
from truckpad.models import DBDriver

Base = declarative_base()


class ModelBase(Base):
    __abstract__ = True

    @property
    def orm(self):
        return DBDriver()

    @staticmethod
    def extract_date(type_cast, field):
        return extract(type_cast, field)
