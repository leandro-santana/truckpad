# coding: utf-8
from sqlalchemy import Column, String
from sqlalchemy.dialects.mysql import INTEGER

from truckpad.models.model_base import ModelBase


class TrucksTypeModel(ModelBase):
    __tablename__ = 'trucks_type'

    id = Column(INTEGER(11), primary_key=True)
    tipo = Column(String(100))
