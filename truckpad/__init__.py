from tornado.web import Application

from .urls import routes


def create_app():
    app = Application(routes)
    return app
