import json


def object_convert(obj_list, json_convert=False):
    """ convert object sqlalchemy to json"""
    result_list = {}
    for num, obj in enumerate(obj_list):
        result_list.update({num: {}})
        for idx, value in enumerate(obj):
            result_list.get(num).update({obj.keys()[idx]: value})
    if json_convert:
        result_list = json.dumps(result_list)

    return result_list


def del_dict_values(dict_values, list_removes):
    """remove keys on dict"""
    for i in list_removes:
        del dict_values[i]
    return dict_values
