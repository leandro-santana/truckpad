import unittest

from tornado.testing import AsyncHTTPTestCase

from truckpad import create_app


class BaseTests(unittest.TestCase):

    def createapp(self):
        return create_app()

    def setUp(self):
        super().setUp()
        self.createapp()

    def tearDown(self):
        super().tearDown()


class BaseHttpTestCase(AsyncHTTPTestCase):
    def get_app(self):
        return create_app()

    def setUp(self):
        super().setUp()

    def tearDown(self):
        super().tearDown()
