import mock

from tests import BaseTests
from truckpad.models.model_base import ModelBase


class TestCreateApp(BaseTests):

    @mock.patch('truckpad.models.model_base.DBDriver')
    def test_instance_orm(self, mock_db_driver):
        model_base = ModelBase()
        model_base.orm
        self.assertTrue(mock_db_driver.called)
