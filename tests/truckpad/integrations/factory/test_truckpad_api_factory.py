import mock
from tests import BaseTests
from truckpad.integrations.factory.truckpad_api_factory import TruckPadApiFactory


class TestCreateApp(BaseTests):

    def test_get_instance_return_class(self):
        instance = TruckPadApiFactory.get_instance(carrier='drivers')
        self.assertTrue(instance._abc_impl)

    @mock.patch('truckpad.integrations.factory.truckpad_api_factory.LIST_ABSTRACTS')
    def test_get_instance_return_http_error(self, mock_list_abstracts):
        with self.assertRaises(Exception) as context:
            TruckPadApiFactory.get_instance(carrier='drivers')
        self.assertTrue(mock_list_abstracts)
        self.assertTrue('HTTP 404: NOT_FOUND' in str(context.exception))
