import mock
from tornado.httpclient import HTTPError
from tests import BaseHttpTestCase
from truckpad.exceptions import ROUTENOTFOUND
from truckpad.views.v1.truckpad_api_view import ApiView


class Instance:
    """ mock Instance de TruckPadApiFactory"""
    contract = None

    @staticmethod
    def process(request):
        return request

    @staticmethod
    def get_data(**kwargs):
        return kwargs


class TestTruckPadApiView(BaseHttpTestCase):
    @mock.patch('truckpad.views.v1.truckpad_api_view.TruckPadApiFactory')
    @mock.patch('truckpad.views.v1.truckpad_api_view.parser')
    def test_http_post_success(self, mock_parser, mock_truckpad_factory):
        ApiView.request = mock.MagicMock(return_value={})
        mock_truckpad_factory.get_instance = mock.MagicMock(return_value=Instance)
        response = self.fetch('/api/drivers', raise_error=False, method="POST", body="")
        self.assertEqual(201, response.code)
        self.assertIn('{"status": "success", "message": "CREATED"}', response.body.decode())
        self.assertTrue(mock_parser.parse.called)
        self.assertTrue(mock_truckpad_factory.get_instance.called)

    @mock.patch('truckpad.views.v1.truckpad_api_view.TruckPadApiFactory')
    def test_http_post_with_http_error(self, mock_truckpad_factory):
        error_code = 422
        error_message = 'http_error'
        mock_truckpad_factory.get_instance().process.side_effect = HTTPError(error_code, error_message)
        with self.assertRaises(Exception) as context:
            self.fetch('/api/drivers', raise_error=True, method="POST", body='')
        self.assertEqual(error_code, context.exception.response.code)
        self.assertEqual('{"status": "error", "message": "http_error"}', context.exception.response.body.decode())
        self.assertEqual(context.exception.response.error.message, 'Unprocessable Entity')

    @mock.patch('truckpad.views.v1.truckpad_api_view.TruckPadApiFactory')
    def test_http_post_with_common_errors(self, mock_truckpad_factory):
        error_code = 404
        error_message = 'ROUTENOTFOUND'
        mock_truckpad_factory.get_instance().process.side_effect = ROUTENOTFOUND(message=error_message,
                                                                                 status_code=error_code)
        with self.assertRaises(Exception) as context:
            self.fetch('/api/drivers', raise_error=True, method="POST", body='')
        self.assertEqual(error_code, context.exception.response.code)
        self.assertEqual('{"status": "error", "message": "ROUTENOTFOUND"}', context.exception.response.body.decode())
        self.assertTrue(mock_truckpad_factory.get_instance.called)

    @mock.patch('truckpad.views.v1.truckpad_api_view.TruckPadApiFactory')
    def test_http_get_success(self, mock_truckpad_factory):
        ApiView.request = mock.MagicMock(return_value={})
        mock_truckpad_factory.get_instance = mock.MagicMock(return_value=Instance)
        response = self.fetch('/api/drivers/full_charge', raise_error=False, method="GET")
        self.assertEqual(200, response.code)
        self.assertTrue(mock_truckpad_factory.get_instance.called)

    @mock.patch('truckpad.views.v1.truckpad_api_view.TruckPadApiFactory')
    def test_http_get_with_http_error(self, mock_truckpad_factory):
        error_code = 500
        error_message = 'http_error'
        mock_truckpad_factory.get_instance().get_data.side_effect = HTTPError(error_code, error_message)
        with self.assertRaises(Exception) as context:
            self.fetch('/api/drivers/full_charge', raise_error=True, method="GET")
        self.assertEqual(error_code, context.exception.response.code)
        self.assertEqual('{"status": "error", "message": "http_error"}', context.exception.response.body.decode())
        self.assertEqual(context.exception.response.error.message, 'Internal Server Error')

    @mock.patch('truckpad.views.v1.truckpad_api_view.TruckPadApiFactory')
    def test_http_get_with_common_errors(self, mock_truckpad_factory):
        error_code = 404
        error_message = 'ROUTENOTFOUND'
        mock_truckpad_factory.get_instance().get_data.side_effect = ROUTENOTFOUND(message=error_message,
                                                                                  status_code=error_code)
        with self.assertRaises(Exception) as context:
            self.fetch('/api/drivers/full_charge', raise_error=True, method="GET")
        self.assertEqual(error_code, context.exception.response.code)
        self.assertEqual('{"status": "error", "message": "ROUTENOTFOUND"}', context.exception.response.body.decode())
        self.assertTrue(mock_truckpad_factory.get_instance.called)
