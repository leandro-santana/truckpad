import mock

from tests import BaseTests
from truckpad import create_app


class TestCreateApp(BaseTests):
    @mock.patch('truckpad.Application')
    def test_create_app(self, mock_application):
        create_app()
        self.assertTrue(mock_application.called)
